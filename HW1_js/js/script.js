let name = prompt('Enter your name', 'Ivan');
let age = prompt('Enter your age', '18');
if (age < 18) {
    alert('You are not allowed to visit this website.');
} else if (age >= 18 && age <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert('Welcome, ' + name);
    } else {
        alert("You are not allowed to visit");
    }
} else {
    alert('Welcome, ' + name);
}


// Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
//     Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok, показать на экране сообщение: Welcome, + имя пользователя. Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст больше 22 лет - показать на экране сообщение: Welcome, + имя пользователя.
//     Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.