// Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
//     При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
//     Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
//     Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать по клику) для данной секции.


$(document).ready(function () {
    $(document).scroll( function (event) {
    const $screenHeight = $(window).innerHeight();
    const $screenTop = $(window).scrollTop();

    if ($screenTop > $screenHeight) {
        if (!$('.scroll-top-btn').length){
            const $scrollTopBtn = $('<button hidden class="scroll-top-btn">Up</button>');

            $('script:first').before($scrollTopBtn);
            $scrollTopBtn.fadeIn();

            $scrollTopBtn.click(() => {
                $('body, html').animate({
                    scrollTop: 0
                }, 1000);
            })
        }
    } else {
        $('.scroll-top-btn').fadeOut(500, () => {
            $('.scroll-top-btn').remove();
        });
    }

    });
        $('.nav-bar>a').click((e) => {
            let clicked = $(e.target)[0].className.split(' ')[1];
            console.dir(clicked);
            switch (clicked) {
                case 'architect':
                    $(document.documentElement).animate({
                        scrollTop: $('.propositions-background').offset().top - 69.5
                    }, 1000);
                    break;
                case 'buildings':
                    $(document.documentElement).animate({
                        scrollTop: $('.propositions-background').offset().top - 69.5
                    }, 1000);
                    break;
                case 'magazine':
                    $(document.documentElement).animate({
                        scrollTop: $('.propositions-title.magazine').offset().top - 69.5
                    }, 1000);
                    break;
                default:
                    $(document.documentElement).animate({
                        scrollTop: $(document.body).offset().top
                    }, 1000);
            }
        });

    const $button = $('<div class="toggle">Скрыть/Показать</div>').css({
        display: 'block',
        fontSize: '40px',
        width: '300px',
        margin: '0 auto',
        textAlign: 'center',
        background: 'red',
        color: 'white',
    });
    $('.propositions-background').after($button);



    function $slideToggle() {
        $button.click((e) => {
            let clickedEl = $(e.target);
            console.log(clickedEl);
            $('.propositions-background').toggle('hidden');
        });
    }
    $slideToggle()
});
