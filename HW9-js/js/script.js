// Реализовать переключение вкладок (табы) на чистом Javascript.
//
//     #### Технические требования:
//     - В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// - Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

let akBtn = document.querySelector('#ak');
let aniBtn = document.querySelector('#ani');
let draBtn = document.querySelector('#dra');
let garBtn = document.querySelector('#gar');
let katBtn = document.querySelector('#kat');

let akaliText = document.querySelector('#akali');
let aniviaText = document.querySelector('#anivia');
let dravenText = document.querySelector('#draven');
let garenText = document.querySelector('#garen');
let katarinaText = document.querySelector('#katarina');


// let classActive = document.querySelector('.active');

let divContent = document.querySelector('.centered-content');
divContent.addEventListener('click', (event) => {
    if (event.target === akBtn) {
        akaliText.hidden = !akaliText.hidden;
        akBtn.classList.toggle('active');
    } else if (event.target === aniBtn) {
        aniviaText.hidden = !aniviaText.hidden;
        aniBtn.classList.toggle('active');
    } else if (event.target === draBtn) {
        dravenText.hidden = !dravenText.hidden;
        draBtn.classList.toggle('active');
    } else if (event.target === garBtn) {
        garenText.hidden = !garenText.hidden;
        garBtn.classList.toggle('active');
    } else if (event.target === katBtn) {
        katarinaText.hidden = !katarinaText.hidden;
        katBtn.classList.toggle('active');
    }

});








