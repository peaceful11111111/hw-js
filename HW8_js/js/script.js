const input = document.createElement('input');
input.setAttribute('type', 'number');
input.setAttribute('id', 'input');
document.querySelector('script').before(input);

const previewText = document.createElement('span');

const uncorrectText = document.createElement('p');
uncorrectText.innerText = `Please enter correct price`;
uncorrectText.style.color ='red';

const removeBtn = document.createElement('button');
removeBtn.innerText = `X`;
removeBtn.classList.add('remove-btn');

const PriceInputName = document.createElement('p');
PriceInputName.innerText = 'Price, $';
PriceInputName.classList.add('Price-Input-Name');
document.querySelector('input').before(PriceInputName);


// debugger;
document.getElementById('input').addEventListener("focus", myFocus);
function myFocus() {
    document.getElementById('input').style.border = "1px solid green";
}
document.getElementById('input').addEventListener('blur', () => {
    if ( input.value <= 0 ){
        input.style.backgroundColor = "";
        input.style.border = "2px solid red";
        previewText.remove();
        document.querySelector('p').before(uncorrectText);
        uncorrectText.append(removeBtn);
    } else {
        input.style.color = 'white';
        input.style.border = "";
        input.style.backgroundColor = "green";
        uncorrectText.remove();
        document.querySelector('p').before(previewText);
        document.querySelector('span').after(removeBtn);
        previewText.innerText = `Текущая цена: ${input.value}`;
    }
});
removeBtn.addEventListener( 'click', () => {
    removeBtn.remove();
    previewText.remove();
    input.value='';
    input.style.backgroundColor = "";
    input.style.border = "";
    uncorrectText.remove();
});


// ## Задание
//
// Создать поле для ввода цены с валидацией.
//
//     #### Технические требования:
//     - При загрузке страницы показать пользователю поле ввода (`input`) с надписью `Price`. Это поле будет служить для ввода числовых значений
// - Поведение поля должно быть следующим:
//     - При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// - Когда убран фокус с поля - его значение считывается, над полем создается `span`, в котором должен быть выведен текст: `Текущая цена: ${значение из поля ввода}`. Рядом с ним должна быть кнопка с крестиком (`X`). Значение внутри поля ввода окрашивается в зеленый цвет.
// - При нажатии на `Х` - `span` с текстом и кнопка `X` должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// - Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - `Please enter correct price`. `span` со значением при этом не создается.
// - В папке `img` лежат примеры реализации поля ввода и создающегося `span`.

