﻿## Теоретический вопрос 

1. Описать своими словами для чего вообще нужны функции в программировании. 
2. Описать своими словами, зачем в функцию передавать аргумент.

ОТВЕТ:

1. Функции нужны для того, чтобы реализовывать какие-то штуки, которые создаются в функции
и возвращать готовые значения в глобальную область видимости, чтобы что-то вывести при 
взаимодействие с пользователем.
2. В функцию передаём аргумент, если он нужен, чтобы при использование функции дать значение
аргументу и проверить так ли работает код внутри функции. Необязательно давать аргументы
в функциях.