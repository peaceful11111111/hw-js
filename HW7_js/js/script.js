function getArray(array) {
    array.map(function callback(currentValue){
        // debugger;
        const li = document.createElement('li');
        document.body.append(li);
        li.innerText = currentValue;
    });
}
getArray(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);

// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
//
//     #### Технические требования:
//     - Создать функцию, которая будет принимать на вход массив.
// - Каждый из элементов массива вывести на страницу в виде пункта списка
// - Необходимо использовать шаблонные строки и функцию `map` массива для формирования контента списка перед выведением его на страницу.
// - Примеры массивов, которые можно выводить на экран:
//     ```javascript
//    ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//    ```
//
//         ```javascript
//    ['1', '2', '3', 'sea', 'user', 23]
//    ```
//     - Можно взять любой другой массив.






