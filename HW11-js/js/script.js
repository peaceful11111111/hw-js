const body = document.querySelector('body');
const buttons = document.querySelectorAll('.btn');

body.addEventListener('keydown', (event) => {
    buttons.forEach((value) => {
        value.classList.remove('blue-bg');
        if (event.key === value.textContent) {
            value.classList.add('blue-bg');
        }
    });
});
// ## Задание
//
// Реализовать функцию подсветки нажимаемых клавиш
//
// #### Технические требования:
//     - В файле `index.html` лежит разметка для кнопок.
// - Каждая кнопка содержит в себе название клавиши на клавиатуре
// - По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию `Enter` первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает `S`, и кнопка `S` окрашивается в синий цвет, а кнопка `Enter` опять становится черной.
// let wrapper = document.querySelector('.btn-wrapper');