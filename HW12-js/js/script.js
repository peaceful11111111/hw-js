const divBtn = document.querySelector('.buttons');
const stopBtn = document.querySelector('#stopBtn');
const continueBtn = document.querySelector('#continueBtn');
const imagesWrapper = document.querySelectorAll('.image-to-show');
console.log(imagesWrapper);

const hideImgs = () => {

    let currentIndex = 0;


    imagesWrapper.forEach((item,index)=>{
        if(item.classList.contains('active')){
            currentIndex=index;
        }
    });

    let currentActiveElem = document.querySelector('.image-to-show.active');

    if(currentIndex!==imagesWrapper.length-1) {
        currentActiveElem.classList.remove('active');
        imagesWrapper[currentIndex+1].classList.add('active')
    } else {
        currentActiveElem.classList.remove('active');
        imagesWrapper[0].classList.add('active')
    }
};

divBtn.addEventListener('click', (event) => {
    if(event.target === stopBtn) {
        clearInterval(timerInterval);
    }
    if (event.target === continueBtn) {
        timerInterval = setInterval(hideImgs, 1000);
    }
});

let timerInterval = setInterval(hideImgs, 1000);

// Реализовать программу, показывающую циклично разные картинки.
//
//     #### Технические требования:
//     - В папке `banners` лежит HTML код и папка с картинками.
// - При запуске программы на экране должна отображаться первая картинка.
// - Через 10 секунд вместо нее должна быть показана вторая картинка.
// - Еще через 10 секунд - третья.
// - Еще через 10 секунд - четвертая.
// - После того, как покажутся все картинки - этот цикл должен начаться заново.
// - При запуске программы где-то на экране должна появиться кнопка с надписью `Прекратить`.
// - По нажатию на кнопку `Прекратить` цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
// - Рядом с кнопкой `Прекратить` должна быть кнопка `Возобновить показ`, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.




