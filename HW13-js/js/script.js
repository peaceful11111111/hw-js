// Реализовать возможность смены цветовой темы сайта пользователем.
//
//     Технические требования:
//
//     Взять любое готовое домашнее задание по HTML/CSS.
//     Добавить на макете кнопку "Сменить тему".
//     При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
//     Выбранная тема должна сохраняться и после перезагрузки страницы

const button = document.querySelector('#change-color-theme');
const div = document.querySelector('.body-content');
const buttons = document.querySelector('.btn');
const table = document.querySelector('table');

function changeThemLocal() {
    button.addEventListener('click', () => {
            div.classList.toggle('new-theme-tbody');

            buttons.classList.toggle('new-theme-buttons');

            table.classList.toggle('new-theme-border');

            if (localStorage.getItem('Theme') === null) {
                localStorage.setItem('Theme', 'change bgc')
            } else {
                localStorage.removeItem('Theme')
            }
    });
    window.addEventListener('load', () => {
        if (localStorage.getItem('Theme') !== null) {
            div.classList.toggle('new-theme-tbody');

            buttons.classList.toggle('new-theme-buttons');

            table.classList.toggle('new-theme-border');
        }
    });
}
changeThemLocal();