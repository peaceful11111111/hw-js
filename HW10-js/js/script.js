const firstInput = document.querySelector('#firstInput');
const secondInput = document.querySelector('#secondInput');
const visibleIcon = document.querySelector('#vis');
const unvisibleIcon = document.querySelector('#unvis');
const button = document.querySelector('#button');
let errorText = document.createElement('p');
button.after(errorText);
errorText.hidden = true;


visibleIcon.addEventListener('click', (event) => {
    if (event.target === visibleIcon) {
        visibleIcon.classList.replace('fa-eye', 'fa-eye-slash');
        firstInput.type = 'password';
    }
});
unvisibleIcon.addEventListener('click', (e) => {
    if (e.target === unvisibleIcon) {
        unvisibleIcon.classList.replace('fa-eye-slash', 'fa-eye');
        secondInput.type = 'text';
    }
});
button.addEventListener('click', (e) => {
    if (e.target === button) {
        if (firstInput.value === secondInput.value && firstInput.value !== '') {
            errorText.hidden = false;
            errorText.innerText = 'Поздравляю!';
            errorText.style.color = 'green';
            alert('You are welcome!')
        } else {
            errorText.hidden = false;
            errorText.innerText = 'Нужно ввести одинаковые значения';
            errorText.style.color = 'red';
        }
    }
});

// Написать реализацию кнопки "Показать пароль".
//
//     Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.