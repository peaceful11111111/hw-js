// Реализовать переключение вкладок (табы) на чистом Javascript.
//
//     Технические требования:
//
//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

const $tabs = $('.tabs');
const $ak = $('#ak');
const $ani = $('#ani');
const $dra = $('#dra');
const $gar = $('#gar');
const $kat = $('#kat');
const $akText = $('#akali');
const $aniviaText = $('#anivia');
const $dravenText = $('#draven');
const $garenText = $('#garen');
const $katarinaText = $('#katarina');
// const $textContent = $('.text-content');

    $tabs.on('click', (event) => {
    $('.active').removeClass('active');
    $(event.target).addClass('active');
            if (event.target === $ak[0]) {
                $akText.attr('hidden', !$akText.attr('hidden'));
            } else if (event.target === $ani[0]) {
                $aniviaText.attr('hidden', !$aniviaText.attr('hidden'));
            } else if (event.target === $dra[0]) {
                $dravenText.attr('hidden', !$dravenText.attr('hidden'));
            } else if (event.target === $gar[0]) {
                $garenText.attr('hidden', !$garenText.attr('hidden'));
            } else if (event.target === $kat[0]) {
                $katarinaText.attr('hidden', !$katarinaText.attr('hidden'));
            }
});